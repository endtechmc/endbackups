# EndBackups backup manager

EndBackups is a system of shell and Python scripts used by EndTech to manage backups of our various servers.

## Our setup

A few notes about our setup, to explain some of the design decisions in this system.

Our physical server has many different users. There is a user for CMP, one for SMP, one for the backups and one for [Endbot](https://github.com/samipourquoi/endbot), our friendly bot overlord.

All of these accounts share a primary group and the server directories as well as the backups directory are readable by this group. This allows the backup user to read the server directories in order to make a backup, as well as allowing the server users to read the backups in order to restore.

This split is to avoid anyone from accidentally deleting everything on our server, as has happened before.
Access to the root user is extremely restricted, and logging in as any other user it is impossible to delete both backups and running servers.
A side effect of this split is that it is impossible to both create and restore backups from one place without breaking the security. Thus the system is split in many scripts.

Eventually, all these different scripts should provide an identical UI to make it seem like one application, with capabilities decided by the current user.

## To Do

- [ ] Upgrade the backup creation script to have UI
- [ ] Read credentials from environment variables or `.env` files
