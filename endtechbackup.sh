#!/usr/bin/env bash

tempdir=$(mktemp -d)
timestamp=$(date --iso-8601=seconds)
echo "Tempdir $tempdir"
echo "Timestamp $timestamp"

cd "$tempdir"

# Copy creative
echo "Broadcasting backup to creative"
mcrcon -P 25576 -p 8vZ2R@nDZnWkEC -w 2 "say Creating backup! Expect a slowdown." save-off "save-all flush"

echo "Copying creative"
cp -r /home/creative/creative .

echo "Enabling saving on creative"
mcrcon -P 25576 -p 8vZ2R@nDZnWkEC -w 2 save-on

echo "Broadcasting backup to survival"
mcrcon -P 25575 -p Fp@oiTWHswC3Wy -w 2 "say Creating backup! Expect a slowdown." save-off "save-all flush"

echo "Copying survival"
cp -r /home/survival/survival .

echo "Enabling saving on survival"
mcrcon -P 25575 -p Fp@oiTWHswC3Wy -w 2 save-on

echo "Compressing creative"
tar --force-local -czf "creative_${timestamp}.tar.gz" ./creative

mv --backup=t "creative_${timestamp}.tar.gz" /home/backup/backups/creative/

echo "Compressing survival"
tar --force-local -czf "survival_${timestamp}.tar.gz" ./survival

mv --backup=t "survival_${timestamp}.tar.gz" /home/backup/backups/survival/

# Cleanup
echo "Cleaning up"
cd
rm -rf "$tempdir"
