#!/usr/bin/env python3

import locale
import os
import tempfile
import shutil
from dialog import Dialog
from time import ctime, sleep
from mcrcon import MCRcon

BACKUP_PATHS = ["/home/e/Documents/endtechbackup"]
BACKUP_EXTENSIONS = (".tar.gz", ".zip")
SERVER_FOLDER = "/home/survival/survival"
RCON_HOST = ""
RCON_PORT = 25576
RCON_PASS = ""

def restore(d):
    files = []
    for path in BACKUP_PATHS:
        files = files + \
        [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and f.endswith(BACKUP_EXTENSIONS)]
    files = sorted(files, key=lambda x: os.path.getmtime(x), reverse=True)

    # Format the options with index, modified date and filename
    options = [(str(idx), ctime(os.path.getmtime(path)) + ": " + os.path.basename(path)) for idx, path in enumerate(files)]
    code, tag = d.menu("Pick a backup:", choices=options)
    if code != d.OK:
        d.msgbox("Aborting")
        return
    path = files[int(tag)]

    d.infobox("Extracting backup to temporary folder")
    tempfolder = tempfile.mkdtemp()
    filename = os.path.basename(path)

    shutil.unpack_archive(path, tempfolder)

    # Detect if there's a subfolder
    tmp_subfolders = [f for f in os.listdir(tempfolder) if os.path.isdir(os.path.join(tempfolder, f))]
    backup_folder = tempfolder
    # Assume that if there's only 1 subfolder, that folder contains the world
    if len(tmp_subfolders) == 1:
        backup_folder = os.path.join(tempfolder, tmp_subfolders[0])

    code = d.yesno("Are you sure?")
    if code != d.OK:
        shutil.rmtree(tempfolder)
        d.msgbox("Aborting")
        return
        
    d.infobox("Contacting server...")
    with MCRcon(RCON_HOST, RCON_PASS, RCON_PORT) as mcr:
        mcr.command("stop")

    sleep(2)
    d.infobox("Moving old world to world_BAK")
    shutil.move(os.path.join(SERVER_FOLDER, "world"), os.path.join(SERVER_FOLDER, "world_BAK"))
    try:
        d.infobox("Copying backup world over")
        shutil.copytree(os.path.join(backup_folder, "world"), os.path.join(SERVER_FOLDER, "world"))
        d.infobox("Removing world_BAK")
        shutil.rmtree(os.path.join(SERVER_FOLDER, "world_BAK"))
        d.infobox("Removing temporary folder")
        shutil.rmtree(tempfolder)
        d.msgbox("Done. Server should auto-restart.")
    except:
        d.infobox("Error occured. Restoring world.")
        shutil.move(os.path.join(SERVER_FOLDER, "world_BAK"), os.path.join(SERVER_FOLDER, "world"))
        d.msgbox("Error recovered. No changes were made.")

    d.msgbox("Returning to main menu")
        

def main():
    locale.setlocale(locale.LC_ALL, '')

    d = Dialog(dialog="dialog", autowidgetsize=True)
    d.set_background_title("EndTech Backup Manager - Restore edition")

    while True:
        code, tag = d.menu("Welcome, what do you want to do?",
                        choices=[
                            ("(1)", "Restore a backup"),
                            ("(2)", "Make a backup"),
                            ("(3)", "Exit"),
                        ])
        if code != d.OK or tag == "(3)":
            return
        if tag == "(1)":
            restore(d)
        elif tag == "(2)":
            d.msgbox("Run as backup user to make a backup.")

if __name__ == "__main__":
    main()
